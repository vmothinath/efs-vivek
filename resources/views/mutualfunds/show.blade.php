@extends('app')
@section('content')
    <h1>Mutualfund </h1>
    <div class="container">
        <table class="table table-striped table-bordered table-hover">
            <tbody>
            <tr class="bg-info">
            
            <tr>
                <td>Fund Name</td>
                <td><?php echo ($mutualfund['fund_name']); ?></td>
            </tr>
            <tr>
                <td>Fund Value</td>
                <td><?php echo ($mutualfund['fund_value']); ?></td>
            </tr>
            <tr>
                <td>Fund Term</td>
                <td><?php echo ($mutualfund['fund_term']); ?></td>
            </tr>
            <tr>
                <td>Fund Date</td>
                <td><?php echo ($mutualfund['fund_date']); ?></td>
            </tr>
            <tr>
                <td>Fund Fee</td>
                <td><?php echo ($mutualfund['fund_fee']); ?></td>
            </tr>
            </tbody>
        </table>
    </div>
@stop
        
