@extends('app')
@section('content')
    <h1>Create New Mutualfund</h1>
    {!! Form::open(['url' => 'mutualfunds']) !!}

       <div class="form-group">
        {!! Form::select('customer_id', $customers) !!}
    </div>

    <div class="form-group">
        {!! Form::label('fund_name', 'Fund Name:') !!}
        {!! Form::text('fund_name',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('fund_value', 'Fund Value:') !!}
        {!! Form::text('fund_value',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('fund_term', 'Fund Term:') !!}
        {!! Form::text('fund_term',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('fund_date', 'Fund Date:') !!}
        {!! Form::text('fund_date',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('fund_fee', 'Fund Fee:') !!}
        {!! Form::text('fund_fee',null,['class'=>'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::submit('Save', ['class' => 'btn btn-primary form-control']) !!}
    </div>
    {!! Form::close() !!}
@stop
