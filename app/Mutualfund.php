<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mutualfund extends Model
{
    //
    protected $fillable=[
        'customer_id',
        'name',
        'fund_name',
        'fund_value',
        'fund_term',
        'fund_date',
        'fund_fee',

    ];

    public function customer() {
        return $this->belongsTo('App\Customer');
    }
}
